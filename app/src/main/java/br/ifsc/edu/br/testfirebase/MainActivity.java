package br.ifsc.edu.br.testfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    DatabaseReference reference;
    FirebaseAuth refeAuth;
    EditText etEmail, etSenha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get database
        reference = FirebaseDatabase.getInstance().getReference();
        refeAuth = FirebaseAuth.getInstance();
        etEmail = findViewById(R.id.et_email);
        etSenha = findViewById(R.id.et_password);

//
//        //add valores
//        reference.child("usuarios").push().setValue(new Usuario("001", "Tai", 16, "tai@gmail.com"));
//        reference.child("usuarios").push().setValue(new Usuario("002", "lor", 18, "lor@gmail.com"));
//        reference.child("usuarios").push().setValue(new Usuario("003", "And", 20, "and@gmail.com"));
//        reference.child("usuarios").push().setValue(new Usuario("004", "Res", 22, "res@gmail.com"));
//
//        //get valores
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                Log.i("FIREBASE:", dataSnapshot.getValue().toString());
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        //recuperar dados por um id especifico
//        DatabaseReference userRef = reference.child("usuarios").child("asdasdasd");

        //recuperar dados com query
        DatabaseReference userRef = reference.child("usuarios");
        //Query userPesquisa = userRef.orderByChild("nome").equalTo("And"); //pelo nome
        Query userPesquisa = userRef.orderByChild("idade").startAt(17).endAt(28).limitToFirst(1000);

        userPesquisa.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                Usuario user = dataSnapshot.getChildren().iterator().next().getValue(Usuario.class);
                Log.i("FIREBASE:", dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //criar user
//        refeAuth.createUserWithEmailAndPassword("tai@gmail.com", "456789")
//                .addOnFailureListener(MainActivity.this, new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.i("USUARIO-NOVO:", e.getMessage());
//                    }
//                }).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
//            @Override
//            public void onComplete(@NonNull Task<AuthResult> task) {
//                if(task.isSuccessful()){
//                    Log.i("USUARIO-NOVO:", "Criado com sucesso" );
//                }else {
//                    Log.i("USUARIO-NOVO:", "Deu ruim!!!" );
//                }
//            }
//        });



        //upload arquivo
        //https://firebase.google.com/docs/storage/android/upload-files?hl=pt-br
    }

    public void fazerLogin(View view){
        String email = etEmail.getText().toString();
        String password = etSenha.getText().toString();

        refeAuth.signOut();

        if (email.equals("") || password.equals("")) {
            return;
        } else {
            Toast.makeText(this, "Login in progress", Toast.LENGTH_SHORT).show();
            refeAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    Log.d("LOGIN", "signInWithEmailAndPassword() onComplete: " + task.isSuccessful());
                    if(task.isSuccessful()){
                        showToast("Bem vindo " + refeAuth.getCurrentUser().getEmail());
                        Intent i = new Intent(MainActivity.this, ViewConversas.class);
                        startActivity(i);
                    } else{
                        showToast("Falha na autenticação");
                    }
                }
            });
        }
    }

    private void showToast(String texto){
        Toast toast=Toast.makeText(getApplicationContext(), texto,Toast.LENGTH_SHORT);
        toast.show();
    }

}
